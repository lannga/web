﻿
const format = (n) => String(n).replace(/(.)(?=(\d{3})+$)/g, '$1,')

const BASE_URL = "api.php"
const SUCCESS_CODE = 0
const postApi = async (data, isFormData = false) => {
    console.log(data)
    let result = null
    const formData = isFormData ? {
        processData: !isFormData,
        contentType: false
    } : {}
    await $.ajax({
        type: "POST",
        url: BASE_URL,
        data: data,
        ...formData
    })
        .done(response => result = response)
        .fail(message => console.log(message))
    console.log(result)
    if (result.code != SUCCESS_CODE) {
        alert(result.message || "LỖi")
        return null
    }
    console.log(result)
    return result.data
}

const logout = () => {
    postApi({
        method: "logout"
    }).then(data => {
        window.location.href = "login.php"
    })
}

const REP_STATUS = {
    0: {
        content: 'Đã tiếp nhận',
        text_color: 'text-warning'
    },
    1: {
        content: 'Đã trả lời',
        text_color: 'text-success'
    },
    2: {
        content: 'Tôi đã huỷ',
        text_color: 'text-danger'
    },
}

const VOTE_STATUS = {
    5: {
        text_color: 'text-warning'
    },
    4: {
        text_color: 'text-warning'
    },
    3: {
        text_color: 'text-warning'
    },
    2: {
        text_color: 'text-danger'
    },
    1: {
        text_color: 'text-danger'
    },
    0: {
        text_color: 'text-secondary'
    },
}


const strUser = sessionStorage.getItem("user")
let user = strUser ? JSON.parse(strUser) : {}
const is_admin = parseInt(user.is_admin)

function renderStar(id, number) {
    let string = ''
    if (is_admin) {
        if (number == 0)
            return `<p class="text-secondary">Khách hàng chưa đánh giá</p>`
        string += `<p class="text-secondary"> Khách hàng ${number < 3 ? "KHÔNG" : ""} hài lòng (${number} sao)</p>`
    }
    else
        for (let i = 0; i < 5; i++) {
            string += i < number ? `<i onclick="vote(${id}, '${i + 1}')" class="fas fa-star text-warning"></i>` : `<i onclick="vote(${id}, '${i + 1}')" class="far fa-star"></i>`
        }
    return string
}

async function vote(id, no) {
    await updateQuestion(id, 'VOTE', no)
    getQuestionList()
}

const getMyQuestionList = (is_admin = true) => {
    $('#my-question-list').empty()
    MY_SAMPLE_DATA.forEach(item =>
        $('#my-question-list').append(`
        <li id=${item.id} class ="border-bottom px-2">
            <div class="d-flex flex-row">
                <p class="text-small pt-1">20-11-2019</p>
                <p class="flex-1 pl-3">${item.question}</p>
                ${!is_admin ? `
                <p class="text-small pt-1 ${REP_STATUS[item.rep_status].text_color}">${REP_STATUS[item.rep_status].content}</p>
                `:
                `
                <p class="text-small pt-1 ${VOTE_STATUS[3].text_color}">${renderStar(3)}</p>
                `
            }
            </div>
            ${item.rep_status === 'REPLIED' ?
                `<p class="bg-light-opacity rounded p-2 p-md-3">
                    <span class="font-italic text-small">Trả lời vào ngày 12-10-2020:</span><br>
                    <span>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                        Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea
                        proident...
                    </span>
                </p>`
                : is_admin ? `
                    <textarea id="reply" placeholder="Trả lời khách hàng" class="form-control" aria-label="With textarea"></textarea>
                    <button onclick="updateQuestion()" class="text-small btn btn-primary mt-3 border-0" >Gửi</button>
                ` : ``
            }
            ${!is_admin && item.rep_status == 'WAITING' ? `
            <div class="d-flex flex-row justify-content-end">
                <button class="text-small btn btn-outline-secondary border-0" >Sửa</button>
                <button type="button" class="text-small btn btn-outline-secondary border-0">Xoá</button>
            </div>
            `
                : ''
            }
        </li>    
    `))
}

//User
function alertUserSuccess(message, hideForm = false) {
    hideForm && $('#form-update').toggleClass('d-flex')
    getUserList()

}

function showUserForm(id, name, email, birthday, address, phone_number, title = "Thêm khách hàng", noToggle) {
    if (!window.location.pathname.includes('user-info'))
        $('#form-update').toggleClass('d-flex')
    $('#form-update').empty()
    $('#form-update').append(`
    <div class="bg-light w-75 p-4 border-lg-round">
        <p class="h3 pb-5">${title}</p>
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Tên</label>
            <div class="col-sm-10">
                <input type="text" placeholder="Nhập tên đầy đủ" class="form-control" aria-required="true" id="name" value="${name || ''}">
            </div>
        </div>        
        <div class="form-group row">
            <label for="phone_number" class="col-sm-2 col-form-label">Số điện thoại</label>
            <div class="col-sm-10">
                <input type="tel" placeholder="Nhập số điện thoại" class="form-control" aria-required="true" id="phone_number" value="${phone_number || ""}">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" placeholder="Nhập email" class="form-control" aria-required="true" id="email"  value="${email || ""}">
            </div>
        </div>
        ${!id ?
            `
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Mật khẩu</label>
                <div class="col-sm-10">
                    <input type="password" placeholder="Nhập mật khẩu" class="form-control" aria-required="true" id="password"  value="">
                </div>
            </div>
            `
            : ''
        }
        <div class="form-group row">
            <label for="birthday" class="col-sm-2 col-form-label">Ngày sinh</label>
            <div class="col-sm-10">
                <input type="text" placeholder="YYYY-MM-DD" class="form-control" id="birthday"  value="${birthday || ""}">
            </div>
        </div>

        <div class="form-group row">
            <label for="address" class="col-sm-2 col-form-label">Địa chỉ</label>
            <div class="col-sm-10">
                <input type="text" placeholder="Nhập địa chỉ" class="form-control" id="address"  value="${address || ""}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button onclick="updateUser(${id})" type="submit" class="btn btn-primary">${id ? 'Cập nhật' : 'Xác nhận'}</button>
            </div>
        </div>
    </div>  
            `
    )

}

function closeForm(){
    console.log('abc')
    $('#pass-update').toggleClass('d-flex')
}

function showPassForm() {
    $('#pass-update').toggleClass('d-flex')
    $('#pass-update').empty()
    $('#pass-update').append(`
    <div class="bg-light w-75 p-4 border-lg-round">
        <div class="text-secondary cursor-pointer mb-3 ml-auto" style="width:20px"><i onclick="closeForm()" class="fas fa-window-close"></i></div>
        <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label">Mật khẩu cũ</label>
            <div class="col-sm-8">
                <input type="password" placeholder="Nhập mật khẩu cũ" class="form-control" aria-required="true" id="old-pass"  value="">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label">Mật khẩu mới</label>
            <div class="col-sm-8">
                <input type="password" placeholder="Nhập mật khẩu mới" class="form-control" aria-required="true" id="password"  value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label">Xác nhận Mật khẩu</label>
            <div class="col-sm-8">
                <input type="password" placeholder="Nhập lại mật khẩu" class="form-control" aria-required="true" id="re-password"  value="">
            </div>
        </div>
        
        <div class="form-group row">
            <div class="col-sm-10">
                <button onclick="changePassword()" type="submit" class="btn btn-primary">Xác nhận</button>
            </div>
        </div>
    </div>  
            `
    )
}

function changePassword() {
    postApi({
        method: 'change-password',
        password: $("#password")?.val(),
        old_password: $("#old-pass")?.val(),
        re_password: $("#re-password")?.val(),
    }).then(data => {
        if(!data)
            return
        $('#pass-update').toggleClass('d-flex')
        alert("Đổi mật khẩu thành công")
    })
}

function updateUser(id) {
    const input ={
        name: $("#name").val(),
        email: $("#email").val(),
        birthday: $("#birthday").val(),
        password: $("#password")?.val(),
        address: $("#address").val(),
        phone_number: $("#phone_number").val(),
    }
    postApi({
        method: id ? 'update-user' : 'insert-user',
        id,
        name: $("#name").val(),
        email: $("#email").val(),
        birthday: $("#birthday").val(),
        password: $("#password")?.val(),
        address: $("#address").val(),
        phone_number: $("#phone_number").val(),
    }).then(data => {
        if(id){
            showUserForm(user.id, input.name, input.email, input.birthday, input.address, input.phone_number, "Thông tin", true)
        }
        alertUserSuccess(!id ? "Thêm Khách Hàng Thành Công" : "Cập nhật thông tin khách hàng thành công", true)
    })
}

function deleteUser(id) {
    postApi({
        method: 'delete-user',
        id
    }).then(data => {
        alertUserSuccess("Xoá khách hàng thành công")
    })
}

function getUserList() {
    $('tbody').empty()
    postApi({ method: 'get-user-list' }).then(data => {
        data.forEach((data) => {
            const { id, name, email, birthday, address, phone_number } = data
            $('tbody').append(`
                <tr>
                    <th scope="row">${id}</th>
                    <td>${name}</td>
                    <td>${address}</td>
                    <td>${phone_number}</td>
                    <td>${birthday}</td>
                    <td>${email}</td>
                    <td>
                        <button class="btn btn-info" onclick="showUserForm(${id},'${name}','${email}','${birthday}','${address}','${phone_number}');">Sửa</button>
                        <button type="button" class="btn btn-danger" onclick="deleteUser(${id});">Xoá</button>
                    </td>
                </tr>   
            `
            )
        })

    })
}

//Question

function alertQuestionSuccess(message, hideForm = false) {
    getQuestionList()
    hideForm && $('#form-update').toggleClass('d-flex')
}

function showQuestionForm(id, title) {
    $('#form-update').toggleClass('d-flex')
    $('#form-update').empty()
    $('#form-update').append(`
    <form class="bg-light w-75 h-75">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Câu hỏi</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" aria-required="true" id="title" value="${title || ""}">
            </div>
        </div>  
        
        <div class="form-group row">
            <div class="col-sm-10">
                <button onclick="updateQuestion(${id}, 'TITLE')" type="submit" class="btn btn-primary">Xác nhận</button>
            </div>
        </div>
    </form>  
            `
    )
}

function getQuestionList() {
    $('#question-list').empty()
    const data = {
        user_id: $("#user_id").val(),
        vote: $("#vote").val(),
        status: $("#status").val(),
        from_date: $("#from-date").val(),
        to_date: $("#to-date").val(),
    }
    postApi({ method: 'get-question-list', ...data }).then(data => {
        if (!data) return
        const { list, total, un_reply, under_3, today } = data
        $("#un_reply").text(un_reply)
        $("#total").text(total)
        $("#under_3").text(under_3)
        $("#today").text(today)
        list.forEach((data) => {
            const { id, title, create_time, content, vote, reply, reply_time, status, admin_id, admin_name, user_name, user_id } = data
            $('#question-list').append(`
            <li id=${id} class ="border-bottom px-2">
            <div class="d-flex flex-row">
                <p class="text-small pt-1">${create_time}</p>
                <p class="flex-1 pl-3">${title}</p>
                ${is_admin ? `
                <p class="text-small pt-1"><span class="text-x-small font-italic text-secondary">Khách hàng : </span> ${user_name.toUpperCase()}(#${user_id})</p>
                `: status == 0 ? `
                <div class="d-flex flex-row justify-content-end">
                    <button onclick="showQuestionForm(${id})" class="text-small btn btn-outline-secondary border-0" >Sửa</button>
                    <button onclick="deleteQuestion(${id})" type="button" class="text-small btn btn-outline-secondary border-0">Xoá</button>
                </div>
                `
                        : ''
                }
            </div>
            ${status == 1 ?
                    `<p class="bg-light-opacity rounded p-2 p-md-3">
                    <span class="text-small"> N/v hỗ trợ ${admin_name.toUpperCase()}(#${admin_id}) trả lời ngày ${reply_time}:</span><br>
                    <span class="font-italic">
                        ${reply}
                    </span>
                </p>`
                    : is_admin ? `
                    <textarea id="reply" placeholder="Trả lời khách hàng" class="form-control" aria-label="With textarea"></textarea>
                    <button onclick="updateQuestion(${id},'REPLY')" class="text-small btn btn-outline-secondary border-0" >Gửi</button>

                ` : ``
                }
            ${status == 1 ? renderStar(id, vote) : ''}
            </li>  
            `
            )
        })
    })
}

async function updateQuestion(id, type, vote) {
    let data = {}
    switch (type) {
        case 'VOTE':
            data = {
                vote,
            }
            break
        case 'REPLY':
            data = {
                reply: $("#reply").val(),
            }
            break
        default:
            data = {
                title: $("#question-title").val(),
            }
            break
    }
    await postApi({
        method: id ? 'update-question' : 'insert-question',
        id,
        ...data
    }).then(data => {
        // if (!data)
        //     return
        $("#question-title").val("")
        alertQuestionSuccess(!id ? "Câu hỏi đã được gửi tới nhân viên hỗ trợ" : "Cập nhật câu hỏi thành công")
    })
}

function deleteQuestion(id) {
    postApi({
        method: 'delete-question',
        id
    }).then(data => {
        if (!data)
            return
        alertQuestionSuccess("Xoá câu hỏi thành công")
    })
}

function initNavigate() {
    if (is_admin == 0) {
        $('#user-nav').hide()
        $('#search-form > div').toggleClass('col-md-6')
        $('#search-form > div:first-child').hide()
        $('#info-nav').show()
        $('#question-form').toggleClass("d-flex")
        if (window.location.pathname.includes('user-info'))
            showUserForm(user.id, user.name, user.email, user.birthday, user.address, user.phone_number, "Thông tin", true)
    }
}

initNavigate()