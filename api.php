<?php
// include 'session.php';
include 'connect.php';

function buildData($data){
    header('Content-Type: application/json');
    return json_encode(array(
        "code" => 0,
        "data" => $data
    ));
}

function buildError($message){
    header('Content-Type: application/json');
    return json_encode(array(
        "code" => 1,
        "message" => $message
    ));
}

$method = $_POST["method"];
session_start();
$user = isset($_SESSION["login_user"]) ? $_SESSION["login_user"] : array();
switch ($method) {
    case "login":
        $password=md5($_POST["password"],false);
        $pe=$_POST["phone_number_or_email"];
        $res=mysqli_query($conn, "SELECT * FROM user where ( phone_number='$pe' OR email='$pe') AND password='$password'");
        // echo("SELECT * FROM user where phone_number='$pe' OR email='$pe' AND password='$password'");
        $rowcheck= mysqli_num_rows($res);
        if($rowcheck <=0 )
            echo buildError("Sai tài khoản/mật khẩu");
        else{
            while($row=$res->fetch_assoc()){
                $_SESSION['login_user'] = $row;
                $data = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "birthday" => $row['birthday'],
                    "phone_number" => $row['phone_number'],
                    "email" => $row['email'],
                    "address" => $row['address'],
                    "is_admin" => $row['is_admin']
                );
                echo buildData($data); 
            }
        }
            
       break;
    case "logout":
    	session_destroy();
        echo buildData("Success");        
        break;
    case "change-password":
        $old = md5($_POST['old_password'], false);
        if($_POST["re_password"]!=$_POST["password"] || $old != $user["password"] ){
            echo buildError("Mật khẩu không trùng hoặc mặt khẩu cũ không đúng");
        }
        else{
            $password=md5($_POST["password"],false);
            $sql1 = "
            UPDATE `user` SET `password`='".$password."' 
            WHERE  `id`='".$user["id"]."'";
            $user["password"] = $password;
            $conn->query($sql1);
            echo buildData("Success");    
        }    
        break;
    case "insert-user":
        $password=md5($_POST["password"],false);
        $sql1 = "
        INSERT INTO `user` (`phone_number`,`password`, `email`, `birthday`, `address`, `name`, `is_admin`) 
        VALUES ('".$_POST["phone_number"]."', '".$password."', '".$_POST["email"]."', '".$_POST["birthday"]."', '".$_POST["address"]."', '".$_POST["name"]."', '0')";
        $conn->query($sql1);
        echo(buildData(null));
        break;
    case "update-user":
        $sql1 = "
        UPDATE `user` SET `phone_number`='".$_POST["phone_number"]."',`email`='".$_POST["email"]."', `birthday`='".$_POST["birthday"]."', `address` = '".$_POST["address"]."', `name` = '".$_POST["name"]."' 
        WHERE  `id`='".$_POST["id"]."'";
        $conn->query($sql1);
        echo(buildData("Success"));
        break;
    case "delete-user":
        $sql1 = "DELETE FROM user WHERE `id`='".$_POST["id"]."';";
        $conn->query($sql1);
        echo(buildData(null));
        break;
    case "get-user-list":
        $res=mysqli_query($conn, "SELECT * FROM user where is_admin='0'");
        $rowcheck= mysqli_num_rows($res);
        if($rowcheck <=0 )
            echo buildError("Không có dữ liệu");
        else{
            $data = array();
            while($row=$res->fetch_assoc()){
                $data[] = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "birthday" => $row['birthday'],
                    "phone_number" => $row['phone_number'],
                    "email" => $row['email'],
                    "address" => $row['address'],
                );
            } 
            echo buildData($data); 
        }
        break;
    case "insert-question":
        $create_time=date("Y-m-d");
        $title = $conn->real_escape_string($_POST["title"]);
        $sql1 = "
        INSERT INTO `question` (`title`,`create_time`, `user_id`)
        VALUES ('".$title."', '".$create_time."','".$user["id"]."')";
        // echo($sql1);
        $conn->query($sql1);
        echo(buildData("Success"));
        break;
    case "get-question-list":
        $query = "SELECT question.*, admin.name as admin_name, user.name as user_name FROM question 
        LEFT JOIN user as admin ON question.admin_id = admin.id 
        LEFT JOIN user ON question.user_id = user.id 
        where 1=1 ";
        if(!$user["is_admin"]){
            $query .= "AND user_id='".$user["id"]."' ";
        }
        if($_POST["status"] !=""){
            $query .= "AND status = '".$_POST["status"]."' ";
        }
        if(!empty($_POST["vote"])){
            $query .= "AND vote ".($_POST["vote"] > 1 ? ">=":"<")."'3' ";
        }
        if(!empty($_POST["user_id"])){
            $query .= "AND user_id = '".$_POST["user_id"]."' ";
        }
        if(!empty($_POST["from_date"])){
            $query .= "AND created_time >= '".$_POST["from_date"]."' ";
        }
        if(!empty($_POST["to_date"])){
            $query .= "AND created_time <= '".$_POST["to_date"]."' ";
        }
        $query .= " ORDER BY ".($user["is_admin"] ? "status ASC, ":"")."id DESC ";
        // echo($query);
        $res=mysqli_query($conn, $query);
        $rowcheck= mysqli_num_rows($res);
        if($rowcheck <=0 ){
            echo buildError("Không có dữ liệu");
            break;
        }
        $result1=mysqli_query($conn,"SELECT COUNT(id) AS total FROM question where 1=1 ".(!$user["is_admin"] ? " AND user_id='".$user["id"]."' " :""));
        $total=array();
        while($row=$result1->fetch_assoc()){
            $total = $row;
        }        
        $result2=mysqli_query($conn,"SELECT COUNT(id) AS un_reply FROM question where status ='0'".(!$user["is_admin"] ? " AND user_id='".$user["id"]."' " :""));
        $un_reply=array();
        while($row=$result2->fetch_assoc()){
            $un_reply = $row;
        }
        $result3=mysqli_query($conn,"SELECT COUNT(id) AS under_ba FROM question where vote >0 and vote < 3".(!$user["is_admin"] ? " AND user_id='".$user["id"]."' " :""));
        $under_3=array();
        while($row=$result3->fetch_assoc()){
            $under_3 = $row;
        }
        $td = date("Y-m-d");
        $result4=mysqli_query($conn,"SELECT COUNT(id) AS today FROM question where create_time = '".$td."'".(!$user["is_admin"] ? " AND user_id='".$user["id"]."' " :""));
        $today=array();
        while($row=$result4->fetch_assoc()){
            $today = $row;
        }
        $list = array();
        while($row=$res->fetch_assoc()){
            $list[] = array(
                "id" => $row['id'],
                "title" => $row['title'],
                "create_time" => $row['create_time'],
                "status" => $row['status'],
                "reply" => $row['reply'],
                "reply_time" => $row['reply_time'],
                "vote" => $row['vote'],
                "admin_id" => $row['admin_id'],
                "user_id" => $row['user_id'],
                "admin_name" => $row['admin_name'],
                "user_name" => $row['user_name'],
            );
        } 
        // $data = ;
        // // echo($data);
        echo buildData(array(
            "list" => $list,
            "total" => $total['total'],
            "un_reply" => $un_reply['un_reply'],
            "under_3" => $under_3['under_ba'],
            "today" => $today['today'],
        ));        
        break;
    case "update-question":
        $sql1 = "UPDATE `question` SET ";
        if(isset($_POST["title"])){
            $title = $conn->real_escape_string($_POST["title"]);
            $sql1 .="`title`='".$title."' ";
        }
        if(isset($_POST["vote"])){
            $sql1 .="`vote`='".$_POST["vote"]."' ";
        }
        if(isset($_POST["reply"])){
            $reply_time=date("Y-m-d");
            $reply = $conn->real_escape_string($_POST["reply"]);

            $sql1 .="`reply`='".$reply."',`status`='1', `reply_time`='".$reply_time."', `admin_id`='".$user['id']."'";
        }        
        $sql1 .="WHERE  `id`='".$_POST["id"]."'";
        $conn->query($sql1);
        echo(buildData(null));
        break;
    case "delete-question":
        $sql1 = "DELETE FROM question WHERE `id`='".$_POST["id"]."';";
        $conn->query($sql1);
        echo(buildData("Success"));
        break;
    default:
        break;
}

?>