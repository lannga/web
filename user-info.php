<?php
    include 'session.php';
?>
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <title>Quản lý khách hàng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>


<body class="h-100 d-flex flex-column">

    <div class="container">
        <nav class="navbar navbar-expand-sm navbar-light bg-white">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="index.html">LOGO</a>
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a id="question-nav" class="nav-link disabled" href="question-list.php">Quản lý câu hỏi</a>
                    </li>
                    <li id="user-nav" class="nav-item">
                        <a  class="nav-link disabled" href="user-list.php">Quản lý khách hàng</a>
                    </li>    
                    
                    <li id="info-nav" class="nav-item"  style="display:none">
                        <a  class="nav-link disabled" href="user-info.php">Thông tin cá nhân</a>
                    </li>
                    
                    <li class="nav-item active">
                        <a href="#" id="logout" class="nav-link disabled" onclick="logout()">Đăng xuất</a>
                    </li>                    
                </ul>
            </div>
        </nav>
    </div>

    <div id='form-update' class="flex-1 d-flex bg-light justify-content-center align-items-center">
         
    </div> 
    <p class="text-primary text-center bg-light cursor-pointer" onclick="showPassForm()">Đổi mật khẩu</p>  

    <div class="w-100 py-3">
        <footer class="container">
            <a href="index.php">
                LOGO
            </a>
            <div class="d-flex justify-content-end align-items-center text-secondary">
                <p class="pr-3 text-x-small ">Hotline: <u>1900 1009</u></p>
                <p class="pr-3 text-x-small ">Email: <u>Hotro@email.com</u></p>
                <p>
                    <i class="fab fa-facebook mr-1"></i>
                    <i class="fab fa-twitter"></i>
                </p>
            </div>

        </footer>
    </div>

    <div id='pass-update' class="position-absolute w-100 h-100 bg-blur d-none justify-content-center align-items-center">
         
    </div>


    

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>