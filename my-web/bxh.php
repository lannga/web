<?php session_start();?>
			<div class="row justify-content-center">
				<div class="col-md-11 bg light mt-2 rounded pb3">
					<h1 class="text-primary p2">BẢNG XẾP HẠNG</h1>
					<!-- <div align="right" style="margin-bottom: 5px";>
						<button type="button" name="add" id="add" class="btn btn-success btn-xs" id ="addbtn" onclick="themsv()">Thêm sinh viên</button>
					</div> -->
					<hr>
					<!-- <div class="form-inline">
						<label for="search" class="font-weight-bold lead text-dark">Nhập tên sinh viên</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="search" id="search_textbxh" class="form-control form-control-lg rounded-0 border-primary" placeholder="Tìm kiếm...">
					</div>
					<hr> -->
					<?php
						include 'connect.php';
						$sql="SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.chuyennganh,sinhvien.lop,ROUND(SUM(diem.diemtongket*mon.sotinchi)/SUM(mon.sotinchi),2) as diemtichluy FROM sinhvien,mon,diem where sinhvien.idsinhvien=diem.idsinhvien AND diem.idmon=mon.idmon group by sinhvien.idsinhvien order by diemtichluy desc";
						$st=$conn->prepare($sql);
						$st->execute();
						$result=$st->get_result();
					?>
					<table class="table table-hover table-light table-striped" id=table-databxh>
						<thead>
							<tr>
								<th class="text-center" style="vertical-align: top;">RANK</th>
								<th class="text-center" style="vertical-align: top;">ID sinh viên</th>
								<th class="text-center" style="vertical-align: top;">Tên sinh viên</th>
								<th class="text-center" style="vertical-align: top;">Chuyên ngành</th>
								<th class="text-center" style="vertical-align: top;">Lớp</th>
								<th class="text-center" style="vertical-align: top;">Điểm tích lũy</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$i=1;
								while($row=$result->fetch_assoc()){ ?>
								<tr>
									<td><input type="text" class="form-control" id="rank<?php echo $i?>" value="<?php echo $i?>" disabled></td>
									<td><input type="text" class="form-control" id="idSV<?php echo $i?>" value="<?php echo $row['idsinhvien']?>" disabled></td>
									<td width="200px"><input type="text" class="form-control" id="tenSV<?php echo $i?>" value="<?php echo $row['tensv']?>"></td>
									<td width="200px"><input type="text" class="form-control" id="chuyennganh<?php echo $i?>" value="<?php echo $row['chuyennganh']?>" disabled></td>
									<td width="200px"><input type="text" class="form-control" id="lop<?php echo $i?>" value="<?php echo $row['lop']?>" disabled></td>
									<td><input type="text" class="form-control" id="diem<?php echo $i?>" value="<?php echo $row['diemtichluy']?>" disabled></td>
								</tr>
						<?php $i=$i+1;}?>
						</tbody>
					</table>
				</div>
			</div>
<!-- 			<script type="text/javascript">
			$(document).ready(function(){
				$("#search_textbxh").keyup(function(){
					var search = $(this).val();
					$.ajax({
						url:'actionQL.php',
						method:'post',
						data:{query:search,bxh:1},
						success:function(response){
							$("#table-databxh").html(response);
						}
					});
				}); -->