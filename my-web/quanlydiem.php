<?php session_start();?>

			<div class="row justify-content-center">
				<div class="col-md-11 bg light mt-2 rounded pb3">
					<h1 class="text-primary p2">Quản lý điểm</h1>
					<div align="right" style="margin-bottom: 5px";>
						<button type="button" name="add" id="add" class="btn btn-success btn-xs" id ="addbtn" onclick="themdiem()">Thêm điểm</button>
					</div>
					<div id="themdiem">
						
					</div>
					<hr>
					<div class="form-inline">
						<label for="search" class="font-weight-bold lead text-dark">Nhập tên sinh viên/tên môn học</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="search" id="search_textdiem" class="form-control form-control-lg rounded-0 border-primary" placeholder="Tìm kiếm...">
					</div>
					<hr>
					<?php
						include 'connect.php';
						$sql="SELECT sinhvien.idsinhvien,sinhvien.tensv,mon.IDmon,mon.tenmon,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky,(diem.diemcc*0.1+diem.diemgiuaky*0.1+diem.diembtl*0.2+diem.diemcuoiky*0.6) as diemtongket FROM mon,sinhvien,diem WHERE sinhvien.idsinhvien=diem.idsinhvien AND diem.IDmon=mon.IDmon ORDER BY sinhvien.idsinhvien";
						$st=$conn->prepare($sql);
						$st->execute();
						$result=$st->get_result();
					?>
					<table class="table table-hover table-light table-striped" id=table-datadiem>
						<thead>
							<tr>
								<th class="text-center" style="vertical-align: top;">ID sinh viên</th>
								<th class="text-center" style="vertical-align: top;">Tên sinh viên</th>
								<th class="text-center" style="vertical-align: top;">ID môn</th>
								<th class="text-center" style="vertical-align: top;">Tên môn</th>
								<th class="text-center" style="vertical-align: top;">Điểm chuyên cần</th>
								<th class="text-center" style="vertical-align: top;">Điểm giữa kỳ</th>
								<th class="text-center" style="vertical-align: top;">Điểm BTL</th>
								<th class="text-center" style="vertical-align: top;">Điểm cuối kỳ</th>
								<th class="text-center" style="vertical-align: top;">Điểm tổng kết</th>
								<th class="text-center" style="vertical-align: top;">Chức năng</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$i=1;
							while($row=$result->fetch_assoc()){ ?>
							<tr>
								<td><input type="text" class="form-control" id="idSv<?php echo $i?>" value="<?php echo $row['idsinhvien']?>" disabled></td>
								<td width="200px"><input type="text" class="form-control" id="tenSv<?php echo $i?>" value="<?php echo $row['tensv']?>" disabled></td>
								<td><input type="text" class="form-control" id="idMon<?php echo $i?>" value="<?php echo $row['IDmon']?>" disabled></td>
								<td width="200px"><input type="text" class="form-control" id="tenMon<?php echo $i?>" value="<?php echo $row['tenmon']?>" disabled></td>
								<td><input type="text" class="form-control" id="diemcc<?php echo $i?>" value="<?php echo $row['diemcc']?>"></td>
								<td><input type="text" class="form-control" id="diemgiuaky<?php echo $i?>" value="<?php echo $row['diemgiuaky']?>"></td>
								<td><input type="text" class="form-control" id="diembtl<?php echo $i?>" value="<?php echo $row['diembtl']?>"></td>
								<td><input type="text" class="form-control" id="diemcuoiky<?php echo $i?>" value="<?php echo $row['diemcuoiky']?>"></td>
								<td><input type="text" class="form-control" id="diemtongket<?php echo $i?>" value="<?php echo $row['diemtongket']?>" disabled></td>
								<td width="10%">
									<button type="button" name="edit" class="btn btn-primary btn-xs edit" id="editdiembtn<?php echo $i;?>" onclick="capnhatdiem()">Sửa</button>
									<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="deldiembtn<?php echo $i; ?>" onclick="xoadiem()">Xóa</button>
								</td>
							</tr>
						<?php $i=$i+1;}?>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#search_textdiem").keyup(function(){
					var search = $(this).val();
					$.ajax({
						url:'actionQL.php',
						method:'post',
						data:{query:search,diem:1},
						success:function(response){
							$("#table-datadiem").html(response);
						}
					});
				});



				
			});
			
		</script>
		<script type="text/javascript">
			function capnhatdiem(){
			var table = document.getElementById("table-datadiem");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idSv"+index;
					var a2="tenSv"+index;
					var a3="idMon"+index;
					var a4="tenMon"+index;
					var a5="diemcc"+index;
					var a6="diemgiuaky"+index;
					var a7="diembtl"+index;
					var a8="diemcuoiky"+index;					
					var idsv=document.getElementById(a1).value;
					var tensv = document.getElementById(a2).value;
					var idmon = document.getElementById(a3).value;
					var tenmon = document.getElementById(a4).value;
					var diemcc = document.getElementById(a5).value;
					var diemgiuaky = document.getElementById(a6).value;
					var diembtl = document.getElementById(a7).value;
					var diemcuoiky = document.getElementById(a8).value;
					// var cell = row.getElementsByTagName("th")[0];
					// 	var id = cell.innerHTML;
					// 	alert("id:" + id);
					$.post("themsv1.php",{updatediem:1,idsv:idsv,tensv:tensv,idmon:idmon,tenmon:tenmon,diemcc:diemcc,diemgiuaky:diemgiuaky,diembtl:diembtl,diemcuoiky:diemcuoiky},function(data){
						alert("Update thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
		function xoadiem(){
			var table = document.getElementById("table-datadiem");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idSv"+index;
					var a2="idMon"+index
					var idSv=document.getElementById(a1).value;
					var idMon=document.getElementById(a2).value;
					$.post("themsv1.php",{deldiem:1,idSv:idSv,idMon:idMon},function(data){
						alert("Xóa thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
		</script>
		<script type="text/javascript">
			function themdiem(){
				$.post("themdiem.php",function(data){
				// console.log(data);	
					$("#themdiem").html(data);
				});
			}
		</script>

