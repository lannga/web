<?php session_start();?>

			<div class="row justify-content-center">
				<div class="col-md-11 bg light mt-2 rounded pb3">
					<h1 class="text-primary p2">Quản lý môn</h1>
					<div align="right" style="margin-bottom: 5px";>
						<button type="button" name="add" id="add" class="btn btn-success btn-xs" id ="addbtn" onclick="themmon()">Thêm môn</button>
					</div>
					<div id="themmon">
						
					</div>
					<hr>
					<div class="form-inline">
						<label for="search" class="font-weight-bold lead text-dark">Nhập tên môn học</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="search" id="search_textmon" class="form-control form-control-lg rounded-0 border-primary" placeholder="Tìm kiếm...">
					</div>
					<hr>
					<?php
						include 'connect.php';
						$sql="SELECT * FROM mon";
						$st=$conn->prepare($sql);
						$st->execute();
						$result=$st->get_result();
					?>
					<table class="table table-hover table-light table-striped" id=table-datamon>
						<thead>
							<tr>
								<th class="text-center" style="vertical-align: top;">ID</th>
								<th class="text-center" style="vertical-align: top;">Tên môn</th>
								<th class="text-center" style="vertical-align: top;">Số tín chỉ</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i=1;
							while($row=$result->fetch_assoc()){ ?>
							<tr>
								<td><input type="text" class="form-control" id="idmon<?php echo $i?>" value="<?php echo $row['IDmon']?>" disabled></td>
								<td><input type="text" class="form-control" id="tenmon<?php echo $i?>" value="<?php echo $row['tenmon']?>"></td>
								<td><input type="text" class="form-control" id="sotinchi<?php echo $i?>" value="<?php echo $row['sotinchi']?>"></td>
								<td width="10%">
									<button type="button" name="edit" class="btn btn-primary btn-xs edit" id="editmonbtn<?php echo $i;?>" onclick="capnhatmon()">Sửa</button>
									<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="delmonbtn<?php echo $i; ?>" onclick="xoamon()">Xóa</button>
								</td>
								
							</tr>
						<?php $i=$i+1;}?>
						</tbody>
					</table>
				</div>
			</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#search_textmon").keyup(function(){
					var search = $(this).val();
					$.ajax({
						url:'actionQL.php',
						method:'post',
						data:{query:search,mon:1},
						success:function(response){
							$("#table-datamon").html(response);
						}
					});
				});



				
			});
			
		</script>
		<script type="text/javascript">
			function capnhatmon(){
			var table = document.getElementById("table-datamon");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idmon"+index;
					var a2="tenmon"+index;
					var a3="sotinchi"+index;
										
					var idmon=document.getElementById(a1).value;
					var tenmon = document.getElementById(a2).value;
					var sotinchi = document.getElementById(a3).value;
					// var cell = row.getElementsByTagName("th")[0];
					// 	var id = cell.innerHTML;
					// 	alert("id:" + id);
					$.post("themsv1.php",{updatemon:1,idmon:idmon,tenmon:tenmon,sotinchi:sotinchi},function(data){
						alert("Update thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
			function xoamon(){
			var table = document.getElementById("table-datamon");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idmon"+index;
					var idmon=document.getElementById(a1).value;
					$.post("themsv1.php",{delmon:1,idmon:idmon},function(data){
						alert("Xóa thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
		</script>
		<script type="text/javascript">
			function themmon(){
				$.post("themmon.php",function(data){
				// console.log(data);	
					$("#themmon").html(data);
				});
			}
		</script>

