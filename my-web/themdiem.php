<?php 
	session_start();
	if(!isset($_SESSION['login'])){
		header("location:qldtLogin.php");
	}
?>

<div>
	<br/>
	<div class="row">
		<div class="col-md-2">ID sinh viên</div>
		<div class="col-md-10">
			<input type="text" id="idSinhvien" class="form-control" placeholder="Nhập ID sinh viên">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">ID môn học</div>
		<div class="col-md-10">
			<input type="text" id="idMon" class="form-control" placeholder="Nhập ID môn học">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Điểm chuyên cần</div>
		<div class="col-md-10">
			<input type="text" id="diemCC" class="form-control" placeholder="Nhập điểm chuyên cần">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Điểm giữa kỳ</div>
		<div class="col-md-10">
			<input type="text" id="diemGiuaky" class="form-control" placeholder="Nhập điểm giữa kỳ">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Điểm bài tập lớn</div>
		<div class="col-md-10">
			<input type="text" id="diemBtl" class="form-control" placeholder="Nhập điểm bài tập lớn">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Điểm cuối kỳ</div>
		<div class="col-md-10">
			<input type="text" id="diemCuoiky" class="form-control" placeholder="Nhập điểm cuối kỳ">	
		</div>
	</div>
	<br/>
	<div>
		<button type="button" class="btn btn-primary" onclick="themdiem()">Thêm điểm</button>
	</div>
</div>
<script type="text/javascript">
//	$(document).ready(function(){
		function themdiem(){
			var idSinhvien=document.getElementById("idSinhvien").value;
			var idMon=document.getElementById("idMon").value;
			var diemCC=document.getElementById("diemCC").value;
			var diemGiuaky=document.getElementById("diemGiuaky").value;
			var diemBtl=document.getElementById("diemBtl").value;
			var diemCuoiky=document.getElementById("diemCuoiky").value;
			//var diemTongket=diemCC*0.1+diemGiuaky*0.1+diemBtl*0.2+diemCuoiky*0.6;
			$.post("themsv1.php",{insertdiem:1,idSinhvien:idSinhvien,idMon:idMon,diemCC:diemCC,diemGiuaky:diemGiuaky,diemBtl:diemBtl,diemCuoiky:diemCuoiky},function(data){
				//console.log(data);
				alert("Thêm thành công");
			});	
		}
		
//	});
</script>