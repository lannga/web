<?php session_start();
	if(!isset($_SESSION["login"]))
		header("location:qldtLogin.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	  <title>Quản lý đào tạo PTIT</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="style.css">
	  <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

	  <!-- jQuery library -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	  <!-- Popper JS -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	  <!-- Latest compiled JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified CSS -->

	</head>
	<body>
		<!-- Grey with black text -->
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		  	<ul class="navbar-nav">
			    <li class="nav-item active">
			      <a class="nav-link" href="trangchu.php">TRANG CHỦ</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="xemdiem.php">XEM ĐIỂM</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="bxh.php">XEM BẢNG XẾP HẠNG</a>
			    </li>
		    </ul>
		    <ul class="navbar-nav ml-auto mr-2">
        		<li class="nav-item"><a class="nav-link" href="logout.php">Đăng xuất</a></li>
        	</ul>
		</nav>
		<div class="container mt-5 mb-auto">
			<div class="row justify-content-center">
			  <div class="col">
			  	<h2 style="color:red;">WELCOME <?php
				    if(isset($_SESSION["login"])){
				        echo $_SESSION["login"]["taikhoan"]."<br>";
				    }
					?>
				</h2>
				<img src="<?php
					echo $_SESSION["login"]["anh"];
				?>" width="300" height="350" alt="AVARTA">

			  </div>
			  <div class="col"><h3><?php
				    if(isset($_SESSION["login"])){
				        echo "ID: ".$_SESSION["login"]["IDsinhvien"]."<br><br>HỌ TÊN: ".$_SESSION["login"]["tensv"]."<br><br>NGÀY SINH: ".$_SESSION["login"]["ngaysinh"]."<br><br>ĐỊA CHỈ: ".$_SESSION["login"]["diachi"]."<br><br>CHUYÊN NGÀNH: ".$_SESSION["login"]["chuyennganh"]."<br><br>LỚP: ".$_SESSION["login"]["lop"]."<br>";
				    }
					?></h3></div>
			</div>
			
		</div>
	</body>
</html>