<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
	<head>
	  <title>Quản lý đào tạo PTIT</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="style.css">
	  <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

	  <!-- jQuery library -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	  <!-- Popper JS -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	  <!-- Latest compiled JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	  
	</head>
	<body>
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		  	<ul class="navbar-nav">
			    <li class="nav-item active">
			      <a class="nav-link" href="trangchu.php">TRANG CHỦ</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="xemdiem.php">XEM ĐIỂM</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" href="#">XEM BẢNG XẾP HẠNG</a>
			    </li>
		    </ul>
		    <ul class="navbar-nav ml-auto mr-2">
        		<li class="nav-item"><a class="nav-link" href="logout.php">Đăng xuất</a></li>
        	</ul>
		</nav>
		<div class="container mt-5 mb-auto">
			<div class="row justify-content-center">
				<div class="col-md-11 bg light mt-2 rounded pb3">
					<h1 class="text-primary p2">Tìm môn học</h1>
					<hr>
					<div class="form-inline">
						<label for="search" class="font-weight-bold lead text-dark">Kết quả</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="search" id="search_text" class="form-control form-control-lg rounded-0 border-primary" placeholder="Tìm kiếm...">
					</div>
					<hr>
					<?php
						include 'connect.php';
						$sql="SELECT mon.tenmon,mon.sotinchi,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky,(diem.diemcc*0.1+diem.diemgiuaky*0.1+diem.diembtl*0.2+diem.diemcuoiky*0.6) as diemtongket FROM diem,mon 
							WHERE diem.IDsinhvien=".$_SESSION['login']['IDsinhvien']."
							AND mon.IDmon=diem.IDmon";
						$st=$conn->prepare($sql);
						$st->execute();
						$result=$st->get_result();
					?>
					<table class="table table-hover table-light table-striped" id=table-data>
						<thead>
							<tr>
								<th class="text-center" style="vertical-align: top;">STT</th>
								<th class="text-center" style="vertical-align: top;">Tên môn</th>
								<th class="text-center" style="vertical-align: top;">Số tín chỉ</th>
								<th class="text-center" style="vertical-align: top;">Điểm chuyên cần</th>
								<th class="text-center" style="vertical-align: top;">Điểm giữa kỳ</th>
								<th class="text-center" style="vertical-align: top;">Điểm bài tập lớn</th>
								<th class="text-center" style="vertical-align: top;">Điểm cuối kỳ</th>
								<th class="text-center" style="vertical-align: top;">Điểm tổng kết</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i =1;
							while($row=$result->fetch_assoc()){ ?>
							<tr>
								<td><?= $i;?></td>
								<td><?= $row['tenmon']; ?></td>
								<td><?= $row['sotinchi']; ?></td>
								<td><?= $row['diemcc']; ?></td>
								<td><?= $row['diemgiuaky']; ?></td>
								<td><?= $row['diembtl']; ?></td>
								<td><?= $row['diemcuoiky']; ?></td>
								<td><?= $row['diemtongket']; ?></td>
							</tr>
						<?php $i=$i+1;}?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#search_text").keyup(function(){
					var search = $(this).val();
					$.ajax({
						url:'action.php',
						method:'post',
						data:{query:search},
						success:function(response){
							$("#table-data").html(response);
						}
						
					});
				})
			});
		</script>
	</body>
</html>