<?php session_start();?>
		<!-- <div class="container mb-5 mt-5"> -->
			<div class="row justify-content-center">
				<div class="col-md-11 bg light mt-2 rounded pb3">
					<h1 class="text-primary p2">Quản lý sinh viên</h1>
					<div align="right" style="margin-bottom: 5px";>
						<button type="button" name="add" id="add" class="btn btn-success btn-xs" id ="addbtn" onclick="themsv()">Thêm sinh viên</button>
					</div>
					<div id="themsv">
						
					</div>
					<hr>
					<div class="form-inline">
						<label for="search" class="font-weight-bold lead text-dark">Nhập tên sinh viên</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="search" id="search_textsv" class="form-control form-control-lg rounded-0 border-primary" placeholder="Tìm kiếm...">
					</div>
					<hr>
					<?php
						include 'connect.php';
						$sql="SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.ngaysinh,sinhvien.chuyennganh,sinhvien.lop FROM sinhvien";
						$st=$conn->prepare($sql);
						$st->execute();
						$result=$st->get_result();
					?>
					<table class="table table-hover table-light table-striped" id=table-datasv>
						<thead>
							<tr>
								<th class="text-center" style="vertical-align: top;">ID</th>
								<th class="text-center" style="vertical-align: top;">Họ tên</th>
								<th class="text-center" style="vertical-align: top;">Ngày sinh</th>
								<th class="text-center" style="vertical-align: top;">Chuyên ngành</th>
								<th class="text-center" style="vertical-align: top;">Lớp</th>
								<th class="text-center" style="vertical-align: top;">Chức năng</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$i=1;
								while($row=$result->fetch_assoc()){ ?>
								<tr>
									<td><input type="text" class="form-control" id="idsv<?php echo $i?>" value="<?php echo $row['idsinhvien']?>" disabled></td>
									<td><input type="text" class="form-control" id="tensv<?php echo $i?>" value="<?php echo $row['tensv']?>"></td>
									<td><input type="text" class="form-control" id="ngaysinh<?php echo $i?>" value="<?php echo $row['ngaysinh']?>"></td>
									<td><input type="text" class="form-control" id="chuyennganh<?php echo $i?>" value="<?php echo $row['chuyennganh']?>"></td>
									<td><input type="text" class="form-control" id="lop<?php echo $i?>" value="<?php echo $row['lop']?>"></td>
									<td width="10%">
										<button type="button" name="edit" class="btn btn-primary btn-xs edit" id="editsvbtn<?php echo $i ?>" onclick="capnhatsv()" >Sửa</button>
										<button type="button" name="delete" class="btn btn-danger btn-xs delete" id="delsvbtn<?php echo $i ?>" onclick="xoasv()">Xóa</button>
									</td>
								</tr>
						<?php $i=$i+1;}?>
						</tbody>
					</table>
				</div>
			</div>
		<!-- </div> -->
		<script type="text/javascript">
			$(document).ready(function(){
				$("#search_textsv").keyup(function(){
					var search = $(this).val();
					$.ajax({
						url:'actionQL.php',
						method:'post',
						data:{query:search,sv:1},
						success:function(response){
							$("#table-datasv").html(response);
						}
					});
				});
			});
			
		</script>
		<script type="text/javascript">
			function capnhatsv(){
			var table = document.getElementById("table-datasv");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idsv"+index;
					var a2="tensv"+index;
					var a3="ngaysinh"+index;
					var a4="chuyennganh"+index;
					var a5="lop"+index;
										
					var idsv=document.getElementById(a1).value;
					var tensv = document.getElementById(a2).value;
					var ngaysinh = document.getElementById(a3).value;
					var chuyennganh = document.getElementById(a4).value;
					var lop = document.getElementById(a5).value;
					// var cell = row.getElementsByTagName("th")[0];
					// 	var id = cell.innerHTML;
					// 	alert("id:" + id);
					$.post("themsv1.php",{updatesv:1,idsv:idsv,tensv:tensv,ngaysinh:ngaysinh,chuyennganh:chuyennganh,lop:lop},function(data){
						alert("Update thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
		function xoasv(){
			var table = document.getElementById("table-datasv");
			var rows = table.getElementsByTagName("tr");
			for (i = 1; i <= rows.length; i++) {
				var currentRow = table.rows[i-1];
				var createClickHandler = function(row,index) {
				return function() {
					var a1="idsv"+index;					
					var idsv=document.getElementById(a1).value;
					// var cell = row.getElementsByTagName("th")[0];
					// 	var id = cell.innerHTML;
					// 	alert("id:" + id);
					$.post("themsv1.php",{delsv:1,idsv:idsv},function(data){
						alert("Delete thành công");
					});
				};
			};
			currentRow.onclick = createClickHandler(currentRow,i-1);
			}
		}
		</script>
		<script type="text/javascript">
			function themsv(){
				$.post("themsv.php",function(data){
				// console.log(data);	
					$("#themsv").html(data);
				});
			}
		</script>
 