<?php 
	session_start();
	if(!isset($_SESSION['login'])){
		header("location:qldtLogin.php");
	}
?>

<div>
	<br/>
	<div class="row">
		<div class="col-md-2">Họ và tên</div>
		<div class="col-md-10">
			<input type="text" id="tensv" class="form-control" placeholder="Nhập họ tên">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Ngày sinh</div>
		<div class="col-md-10">
			<input type="text" id="ngaysinh" class="form-control" placeholder="Nhập ngày sinh(YYYY-MM-DD)">	
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Địa chỉ</div>
		<div class="col-md-10">
			<input type="text" id="diachi" class="form-control" placeholder="Nhập địa chỉ">
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Giới tính</div>
		<div class="col-md-2">
			<select id="gioitinh" class="form-control">
				<option>Nam</option>
				<option>Nữ</option>
			</select>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Chuyên ngành</div>
		<div class="col-md-5">
			<select id="chuyennganh" class="form-control">
				<option>Công nghệ thông tin</option>
				<option>An toàn thông tin</option>
				<option>Công nghệ đa phương tiện</option>
			</select>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Lớp</div>
		<div class="col-md-10">
			<input type="text" id="lop" class="form-control" placeholder="Nhập lớp">
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Tài khoản</div>
		<div class="col-md-10">
			<input type="text" id="taikhoan" class="form-control" placeholder="Nhập tài khoản">
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2">Mật khẩu</div>
		<div class="col-md-10">
			<input type="password" id="matkhau" class="form-control" placeholder="Nhập mật khẩu">
		</div>
	</div>
	<br/>
	<div>
		<button type="button" class="btn btn-primary" onclick="themsv1()">Thêm sinh viên</button>
	</div>
</div>
<script type="text/javascript">
//	$(document).ready(function(){
		
		function themsv1(){
			var tensv=document.getElementById("tensv").value;
			var ngaysinh=document.getElementById("ngaysinh").value;
			var diachi=document.getElementById("diachi").value
			var gioitinh=document.getElementById("gioitinh").value;
			var chuyennganh=document.getElementById("chuyennganh").value;
			var lop=document.getElementById("lop").value;
			var taikhoan=document.getElementById("taikhoan").value;
			var matkhau=document.getElementById("matkhau").value;
			//console.log(matkhau);
			$.post("themsv1.php",{insertsv:1,tensv:tensv,ngaysinh:ngaysinh,diachi:diachi,gioitinh:gioitinh,chuyennganh:chuyennganh,lop:lop,taikhoan:taikhoan,matkhau:matkhau},function(data){
				console.log(data);
				alert("Thêm thành công");
			});	
		}
		
//	});
</script>

