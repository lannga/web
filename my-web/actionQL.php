<?php
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
	include 'connect.php';
	$output='';
	if(isset($_POST['sv'])){
		if(isset($_POST['query'])){
		$search=$_POST['query'];
		$sql="SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.ngaysinh,sinhvien.chuyennganh,sinhvien.lop FROM sinhvien 
			WHERE sinhvien.tensv LIKE CONCAT('%',?,'%')";
		$st=$conn->prepare($sql);
		$st->bind_param("s",$search);

	}
	else{
		$st=$conn->prepare("SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.ngaysinh,sinhvien.chuyennganh,sinhvien.lop FROM sinhvien");
		}
		$st->execute();
		$result=$st->get_result();

		if($result->num_rows>0){
			$output="<thead>
					<tr>
						<th class=\"text-center\" style=\"vertical-align: top;\">ID</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Họ tên</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Ngày sinh</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Chuyên ngành</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Lớp</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Chức năng</th>
					</tr>
					</thead>
					<tbody>";
					$i=1;
					while($row=$result->fetch_assoc()){
					$output .="
						<tr>
							<td><input type=\"text\" class=\"form-control\" id=\"idsv".$i."\" value=\"".$row['idsinhvien']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"tensv".$i."\" value=\"".$row['tensv']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"ngaysinh".$i."\" value=\"".$row['ngaysinh']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"chuyennganh".$i."\" value=\"".$row['chuyennganh']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"lop".$i."\" value=\"".$row['lop']."\"></td>
							<td>
								<button type="."'button'"." name="."'edit'"." class="."'btn btn-primary btn-xs edit'"." id="."'editsvbtn".$i."' onclick=\"capnhatsv()\" >Sửa</button>
								<button type="."'button'"." name="."'delete'"." class="."'btn btn-danger btn-xs delete'"." id="."'delsvbtn".$i."' onclick=\"xoasv()\">Xóa</button>
							</td>

						</tr>
						
						";
					$i=$i+1;
					}
					$output .="</tbody>";
					echo $output."<script type=\"text/javascript\" src=\"javascript.js\"></script>\n";
		}
		else{
			echo "<h3> Không có kết quả tìm kiếm </h3>";
		}
	}
	if(isset($_POST['mon'])){
		if(isset($_POST['query'])){
		$search=$_POST['query'];
		$st=$conn->prepare("SELECT * FROM mon 
			WHERE mon.tenmon LIKE CONCAT('%',?,'%')");
		$st->bind_param("s",$search);
	}
	else{
		$st=$conn->prepare("SELECT * FROM mon");
		}
		$st->execute();
		$result=$st->get_result();

		if($result->num_rows>0){
			$output="<thead>
					<tr>
						<th class=\"text-center\" style=\"vertical-align: top;\">ID</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Tên môn</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Số tín chỉ</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Chức năng</th>
					</tr>
					</thead>
					<tbody>";
					$i=1;
					while($row=$result->fetch_assoc()){
					$output .="
						<tr>
							<td><input type=\"text\" class=\"form-control\" id=\"IDmon".$i."\" value=\"".$row['IDmon']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"tenmon".$i."\" value=\"".$row['tenmon']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"sotinchi".$i."\" value=\"".$row['sotinchi']."\"></td>
							<td>
								<button type="."'button'"." name="."'edit'"." class="."'btn btn-primary btn-xs edit'"." id="."'editmonbtn".$i."' onclick=\"capnhatmon()\" >Sửa</button>
								<button type="."'button'"." name="."'delete'"." class="."'btn btn-danger btn-xs delete'"." id="."'delmonbtn".$i."' onclick=\"xoamon()\">Xóa</button>
							</td>
						</tr>";
						$i=$i+1;
					}
					$output .="</tbody>";
					echo $output."<script type=\"text/javascript\" src=\"javascript.js\"></script>\n";
					
		}
		else{
			echo "<h3> Không có kết quả tìm kiếm </h3>";
		}
	}
	if(isset($_POST['diem'])){
		if(isset($_POST['query'])){
		$search=$_POST['query'];
		$st=$conn->prepare("SELECT sinhvien.idsinhvien,sinhvien.tensv,mon.idmon,mon.tenmon,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky, diem.diemtongket FROM mon,sinhvien,diem 
			WHERE sinhvien.idsinhvien=diem.idsinhvien AND diem.idmon=mon.idmon AND (mon.tenmon LIKE CONCAT('%',?,'%') OR sinhvien.tensv LIKE CONCAT('%',?,'%') )ORDER BY sinhvien.idsinhvien");
		$st->bind_param("ss",$search,$search);
	}
	else{
		$st=$conn->prepare("SELECT sinhvien.idsinhvien,sinhvien.tensv,mon.IDmon,mon.tenmon,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky,diem.diemtongket FROM mon,sinhvien,diem WHERE sinhvien.idsinhvien=diem.idsinhvien AND diem.idmon=mon.idmon ORDER BY sinhvien.idsinhvien");
		}
		$st->execute();
		$result=$st->get_result();

		if($result->num_rows>0){
			$output="<thead>
					<tr>
						<th class=\"text-center\" style=\"vertical-align: top;\">ID sinh viên</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Tên sinh viên</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">ID môn</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Tên môn</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Điểm chuyên cần</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Điểm giữa kỳ</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Điểm BTL</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Điểm cuối kỳ</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Điểm tổng kết</th>
						<th class=\"text-center\" style=\"vertical-align: top;\">Chức năng</th>
					</tr>
					</thead>
					<tbody>";
					$i=1;
					while($row=$result->fetch_assoc()){
					$output .="
						<tr>
							<td><input type=\"text\" class=\"form-control\" id=\"idSv".$i."\" value=\"".$row['idsinhvien']."\" disabled></td>
							<td width=\"200px\"><input type=\"text\" class=\"form-control\" id=\"tenSv".$i."\" value=\"".$row['tensv']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"idMon".$i."\" value=\"".$row['idmon']."\" disabled></td>
							<td width=\"200px\"><input type=\"text\" class=\"form-control\" id=\"tenMon".$i."\" value=\"".$row['tenmon']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diemcc".$i."\" value=\"".$row['diemcc']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diemgiuaky".$i."\" value=\"".$row['diemgiuaky']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diembtl".$i."\" value=\"".$row['diembtl']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diemcuoiky".$i."\" value=\"".$row['diemcuoiky']."\"></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diemtongket".$i."\" value=\"".$row['diemtongket']."\" disabled></td>
							<td>
								<button type="."'button'"." name="."'edit'"." class="."'btn btn-primary btn-xs edit'"." id="."'editdiembtn".$i."' onclick=\"capnhatdiem()\">Sửa</button>
								<button type="."'button'"." name="."'delete'"." class="."'btn btn-danger btn-xs delete'"." id="."'deldiembtn".$i."' onclick=\"xoadiem()\">Xóa</button>
							</td>

						</tr>";
						
						$i=$i+1;
					}
					$output .="</tbody>";
					echo $output."<script type=\"text/javascript\" src=\"javascript.js\"></script>\n";
					
		}
		else{
			echo "<h3> Không có kết quả tìm kiếm </h3>";
		}
	}
	if(isset($_POST['bxh'])){
		if(isset($_POST['query'])){
		$search=$_POST['query'];
		$st=$conn->prepare("SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.chuyennganh,sinhvien.lop,ROUND(SUM(diem.diemtongket*mon.sotinchi)/SUM(mon.sotinchi),2) as diemtichluy FROM sinhvien,mon,diem where sinhvien.idsinhvien=diem.idsinhvien AND sinhvien.tensv LIKE CONCAT('%',?,'%') AND diem.idmon=mon.idmon group by sinhvien.idsinhvien order by diemtichluy desc");
		$st->bind_param("s",$search);
	}
	else{
		$st=$conn->prepare("SELECT sinhvien.idsinhvien,sinhvien.tensv,sinhvien.chuyennganh,sinhvien.lop,ROUND(SUM(diem.diemtongket*mon.sotinchi)/SUM(mon.sotinchi),2) as diemtichluy FROM sinhvien,mon,diem where sinhvien.idsinhvien=diem.idsinhvien AND diem.idmon=mon.idmon group by sinhvien.idsinhvien order by diemtichluy desc");
		}
		$st->execute();
		$result=$st->get_result();

		if($result->num_rows>0){
			$output="<thead>
					<tr>
								<th class=\"text-center\" style=\"vertical-align: top;\">RANK</th>
								<th class=\"text-center\" style=\"vertical-align: top;\">ID sinh viên</th>
								<th class=\"text-center\" style=\"vertical-align: top;\">Tên sinh viên</th>
								<th class=\"text-center\" style=\"vertical-align: top;\">Chuyên ngành</th>
								<th class=\"text-center\" style=\"vertical-align: top;\">Lớp</th>
								<th class=\"text-center\" style=\"vertical-align: top;\">Điểm tích lũy</th>
					</tr>
					</thead>
					<tbody>";
					$i=1;
					while($row=$result->fetch_assoc()){
					$output .="
						<tr>
							<td><input type=\"text\" class=\"form-control\" id=\"rank".$i."\" value=\"".$i."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"idSV".$i."\" value=\"".$row['idsinhvien']."\" disabled></td>
							<td width=\"200px\"><input type=\"text\" class=\"form-control\" id=\"tenSV".$i."\" value=\"".$row['tensv']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"chuyennganh".$i."\" value=\"".$row['chuyennganh']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"lop".$i."\" value=\"".$row['lop']."\" disabled></td>
							<td><input type=\"text\" class=\"form-control\" id=\"diembtl".$i."\" value=\"".$row['diemtichluy']."\" disabled></td>

						</tr>";
						
						$i=$i+1;
					}
					$output .="</tbody>";
					echo $output;
					
		}
		else{
			echo "<h3> Không có kết quả tìm kiếm </h3>";
		}
	}
?>