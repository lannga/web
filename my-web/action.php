<?php
	session_start();
	include 'connect.php';
	$output='';

	if(isset($_POST['query'])){
		$search=$_POST['query'];
		$st=$conn->prepare("SELECT mon.tenmon,mon.sotinchi,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky,(diem.diemcc*0.1+diem.diemgiuaky*0.1+diem.diembtl*0.2+diem.diemcuoiky*0.6) as diemtongket FROM diem,mon 
			WHERE diem.IDsinhvien=".$_SESSION['login']['IDsinhvien']."
			AND mon.IDmon=diem.IDmon AND mon.ten LIKE CONCAT('%',?,'%')");
		$st->bind_param("s",$search);
	}
	else{
		$st=$conn->prepare("SELECT mon.tenmon,mon.sotinchi,diem.diemcc,diem.diemgiuaky,diem.diembtl,diem.diemcuoiky,(diem.diemcc*0.1+diem.diemgiuaky*0.1+diem.diembtl*0.2+diem.diemcuoiky*0.6) as diemtongket FROM diem,mon 
			WHERE diem.IDsinhvien=".$_SESSION['login']['IDsinhvien']."
			AND mon.IDmon=diem.IDmon");
	}
	$st->execute();
	$result=$st->get_result();

	if($result->num_rows>0){
		$output="<thead>
				<tr>
					<th>STT</th>
					<th>Tên môn</th>
					<th>Số tín chỉ</th>
					<th>Điểm chuyên cần</th>
					<th>Điểm giữa kỳ</th>
					<th>Điểm bài tập lớn</th>
					<th>Điểm cuối kỳ</th>
					<th>Điểm tổng kết</th>
				</tr>
				</thead>
				<tbody>";
				$i=1;
				while($row=$result->fetch_assoc()){
				$output .="
					<tr>
						<td>".$i."</td>
						<td>".$row['tenmon']."</td>
						<td>".$row['sotinchi']."</td>
						<td>".$row['diemcc']."</td>
						<td>".$row['diemgiuaky']."</td>
						<td>".$row['diembtl']."</td>
						<td>".$row['diemcuoiky']."</td>
						<td>".$row['diemtongket']."</td>
					</tr>";
					$i=$i+1;
				}
				$output .="</tbody>";
				echo $output;
	}
	else{
		echo "<h3> Không có kết quả tìm kiếm </h3>";
	}

?>