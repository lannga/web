<?php
      session_start();
      if(isset($_SESSION['login']) and isset($_SESSION['pwd']) and isset($_SESSION["remember"])){
          $user=$_SESSION['login']['taikhoan'];
          $pwd=$_SESSION['pwd'];
          if($user=="admin"){
            header("location:admin.php");
          }
          else{
            header("location:trangchu.php");
          }
          
      }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Quản lý đào tạo PTIT</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="style.css">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
      
      <form class="form-horizontal" action="validate.php" method="POST">
        <h2>ĐĂNG NHẬP</h2>
        <div class="form-group">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
          <label class="control-label col-sm-2" for="acc">Tài khoản:</label>
          <div class="col-sm-10">
              <input type="text" class="form-control" id="account" placeholder="Nhập tài khoản" name="user" value="<?php 
                if(isset($_SESSION["login"])){
                    echo $_SESSION["login"]["taikhoan"];
                }
              ?>">
          </div>
        </div>
        <div class="form-group">
          <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
          <label class="control-label col-sm-2" for="pwd">Mật khẩu:</label>
          <div class="col-sm-10">          
            <input type="password" class="form-control" id="pwd" placeholder="Nhập mật khẩu" name="pwd" value="<?php 
                  if(isset($_SESSION["pwd"])){
                      echo $_SESSION["pwd"];
                  }
              ?>">
          </div>
        </div>
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
              <label><input type="checkbox" name="remember" value="<?php 
                  if(isset($_SESSION["remember"])){
                      echo "checked";
                  }
              ?>"> Duy trì đăng nhập</label>
            </div>
          </div>
        </div>
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success btn-block" name="submit" value="Sent">Đăng nhập</button>
          </div>
        </div>
      </form>
    </div>       

</body>
</html>


