<?php session_start();
	if(!isset($_SESSION["login"]))
		header("location:qldtLogin.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	  <title>Quản lý đào tạo PTIT</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="style.css">
	  <!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

	  <!-- jQuery library -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	  <!-- Popper JS -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	  <!-- Latest compiled JavaScript -->
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- Latest compiled and minified CSS -->

	</head>
	<body>
		<!-- Grey with black text -->
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		  	<ul class="navbar-nav">
			    <li class="nav-item active">
			      <a class="nav-link" href="admin.php">TRANG CHỦ</a>
			    </li>
			
			    <li class="nav-item dropdown">
			      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
			        QUẢN LÝ ĐÀO TẠO
			      </a>
			      <div class="dropdown-menu">
			      	<div class="dropdown-item" onclick="qlsv()" style="cursor: pointer;">Quản lý sinh viên</div>
			      	<div class="dropdown-item" onclick="qlmon()" style="cursor: pointer;">Quản lý môn</div>
			      	<div class="dropdown-item" onclick="qldiem()" style="cursor: pointer;">Quản lý điểm</div>

			      </div>
			    </li>
			
			    <li class="nav-item">
			      <div class="nav-link" onclick="bxh()" style="cursor: pointer;">XEM BẢNG XẾP HẠNG</a>
			    </li>
		    </ul>
		    <ul class="navbar-nav ml-auto mr-2">
        		<li class="nav-item"><a class="nav-link" href="logout.php">Đăng xuất</a></li>
        	</ul>
		</nav>
		<div class="container mt-5 mb-5" id="main">
			<h1>
				HELLO ADMIN!
			</h1>
		</div>
	</body>
	<script type="text/javascript">
		function qlsv(){
			$.post("quanly.php",function(data){
				$("#main").html(data);
			});
		}
		function qlmon(){
			$.post("quanlymon.php",function(data){
				$("#main").html(data);
			});
		}
		function qldiem(){
			$.post("quanlydiem.php",function(data){
				$("#main").html(data);
			});
		}
		function bxh(){
			$.post("bxh.php",function(data){
				$("#main").html(data);
			});
		}
	</script>
</html>