
<?php session_start();
if(isset($_SESSION["login_user"]))
    header("location:question.php")
?>

<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <title>Đăng nhập</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <meta name="google-signin-client_id"
        content="436688189963-h9kjo9j4ifkt4518r0huft9js47anq1k.apps.googleusercontent.com">
    <link rel="stylesheet" href="assets/css/style.css">
</head>


<body class="h-100 d-flex flex-column" style="background-image: url('assets/images/bg.png');background-size:cover">

    <div style="min-width:300px" class="w-50 d-flex flex-column align-items-center py-5 m-auto m-md-0">        
        <img style="width:50px; height:50px" src="assets/images/image.png"/>    
        <p class="text-primary p-3">LOGO</p>          
    </div>

    <div style="min-width:300px" class="w-50 flex-1 m-auto m-md-0">
        <div style="width:300px" class="rounded shadow m-auto py-5 px-4">
                <p class="bold">Đăng nhập tài khoản</p>
                <input type="text" class="form-control my-2 py-2" placeholder="Nhập SĐT/email">
                <input type="password" class="form-control my-3 py-2" placeholder="Nhập mật khẩu">
                <button type="submit" class="btn btn-primary my-2 py-2 w-100">Đăng
                    nhập</button>
                <!-- <a href="#" class="ml-auto">Quên mật khẩu</a> -->
        </div>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Image URL: ' + profile.getPhoneNumber());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        }
    </script>
    <script>
        $("button").click(() => {
            postApi({
                method: "login",
                phone_number_or_email: $("input[type=text]").val(),
                password: $("input[type=password]").val(),
            }).then(data => {
                if(!data) return
                sessionStorage.setItem("user", JSON.stringify(data))
                window.location.href = "question.php"
                
            })
        }); 
    </script>
</body>

</html>