
<?php
    include 'session.php';
?>
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <title>Hỗ trợ khách hàng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id"
        content="436688189963-h9kjo9j4ifkt4518r0huft9js47anq1k.apps.googleusercontent.com">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="h-100 d-flex flex-column">

    <div class="w-100">
        <nav class="navbar navbar-expand-sm navbar-light bg-white">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="d-flex align-items-center">
                    <img style="width:40px; height:40px" src="assets/images/image.png"/>
                    <!-- <p class="text-primary h5">LOGO</p> -->
                </div>
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a id="question-nav" class="nav-link disabled" href="question.php">Quản lý câu hỏi</a>
                    </li>
                    <li id="user-nav" class="nav-item active">
                        <a  class="nav-link disabled" href="user.php">Quản lý khách hàng</a>
                    </li>    
                    
                    <li id="info-nav" class="nav-item active"  style="display:none">
                        <a  class="nav-link disabled" href="user-info.php">Thông tin cá nhân</a>
                    </li>
                    
                    <li class="nav-item active">
                        <a id="logout" class="nav-link disabled" href="#" onclick="logout()">Đăng xuất</a>
                    </li> 
                </ul>
            </div>
        </nav>
    </div>

    <div class="flex-1  w-100" style="background-color:#eee">
        <div class="container-fluid m-auto">
            <div class="row">
                <div class="col-md-3 d-none d-md-flex flex-column my-3">
                    <div class="border-lg-round p-4">
                        <div class="border-m-round overflow-hidden shadow" style="width: 80px;height:80px;">
                            <img style="width: 80px;height:80px;" class="img-fluid"
                                src="assets/images/placeholder.png" />
                        </div>
                        <p class="h3 my-4">Nguyen Thi A</p>
                        <div class="text-muted"><span class="text-center text-x-small"
                                style="display:inline-block; width: 30px"><i class="fas fa-phone"></i></span>098766555
                        </div>
                        <div class="text-muted"><span class="text-center text-x-small"
                                style="display:inline-block; width: 30px"><i
                                    class="fas fa-envelope"></i></span>abc@gmail.com</div>

                        <div class="w-100 border-bottom my-3"></div>
                        <!-- <div class="my-3">
                            <a id="question-nav" class="text-primary center-v disabled" href="question-list.php">QUẢN LÝ CÂU HỎI</a>
                        </div>
                    
                        <div class="my-3">
                            <a  lass="text-muted center-v disabled" href="user-list.php">QUẢN LÝ KHÁCH HÀNG</a>
                        </div>
                    
                        <div class="my-3">
                            <a  lass="text-muted center-v disabled" href="user-info.php">THÔNG TIN CÁ NHÂN</a>
                        </div> -->

                        <div class="p-3">
                        <p class="bold mb-3">Thống kê</p>
                        <p class="d-flex flex-row justify-content-between">Chưa trả lời: <span
                                class="bold" id="un_reply">0</span></p>
                        <p class="d-flex flex-row justify-content-between">Ngày hôm nay: <span
                                class="bold" id="today">0</span></p>
                        <p class="d-flex flex-row justify-content-between">&lt; 3 sao: <span
                                class="bold" id="under_3">0</span></p>
                        <p class="d-flex flex-row justify-content-between">Tổng số: <span
                                class="bold" id="total">0</span></p>
                    </div>
                    

                    </div>

                    

                </div>
                <div class="col-12 col-md-9 my-3">
                    <div class="d-flex flex-row justify-content-between">
                        <!-- <p>Tạo mới <i class="fas fa-plus-circle text-small text-primary"></i></p> -->
                    </div>
                    <div class="border-lg-round p-3 mb-3 d-flex flex-row">
                        <div class="needs-validation w-100">
                            <div id="search-form" class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationTooltip01">Mã khách hàng</label>
                                    <input type="text" placeholder="Tất cả" class="form-control" id="user_id" value=""
                                        >
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationTooltip04">Trạng thái</label>
                                    <select id="status" class="custom-select">
                                        <option value="0" selected>Chưa trả lời</option>
                                        <option value="1">Đã trả lời</option>
                                        <option value="">Tất cả</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationTooltip04">Đánh giá</label>
                                    <select id="vote" class="custom-select">
                                        <option value="" selected>Tất cả</option>
                                        <option value="1"> Dưới 3 sao</option>
                                        <option value="2">Từ 3 sao</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationTooltip01">Từ ngày</label>
                                    <input type="text" placeholder="YYYY-MM-DD" class="form-control" id="from-date"
                                        value="" >
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationTooltip01">Đến ngày</label>
                                    <input type="text" placeholder="YYYY-MM-DD" class="form-control" id="to-date"
                                        value="" >
                                </div>
                            </div>
                            <button onclick="getQuestionList()" class="btn btn-primary" type="submit">Lọc</button>
                        </div>
                    </div>
                    <div id="question-form" class="border-lg-round overflow-hidden d-none my-3">
                        <textarea maxlength="200" id="question-title" class="form-control py-3 border-0" placeholder="Nhập câu hỏi của bạn"
                            rows="2"></textarea>
                        <button onclick="updateQuestion()" type="button" class="btn btn-primary">Gửi câu hỏi</button>
                    </div>
                    <div id="main" class="border-lg-round overflow-hidden">
                        <ul id="question-list" class="cus-list p-3">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="w-100 py-3 pt-2">
        <footer class="container">
            <a href="index.html">
                LOGO
            </a>
            <div class="d-flex justify-content-end align-items-center text-secondary">
                <p class="pr-3 text-x-small ">Hotline: <u>1900 1009</u></p>
                <p class="pr-3 text-x-small ">Email: <u>Hotro@email.com</u></p>
                <p>
                    <i class="fab fa-facebook mr-1"></i>
                    <i class="fab fa-twitter"></i>
                </p>
            </div>

        </footer>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
    

    <script type="text/javascript">
        getQuestionList()
    </script>
</body>

</html>