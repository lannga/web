
<?php
    include 'session.php';
?>
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <title>Hỗ trợ khách hàng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="h-100 d-flex flex-column">

    <div class="w-100">
        <nav class="navbar navbar-expand-sm navbar-light bg-white">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="d-flex align-items-center">
                    <img style="width:40px; height:40px" src="assets/images/image.png"/>
                    <!-- <p class="text-primary h5">LOGO</p> -->
                </div>
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a id="question-nav" class="nav-link disabled" href="question.php">Quản lý câu hỏi</a>
                    </li>
                    <li id="user-nav" class="nav-item">
                        <a  class="nav-link disabled" href="user-list.php">Quản lý khách hàng</a>
                    </li>    
                    
                    <li id="info-nav" class="nav-item active"  style="display:none">
                        <a  class="nav-link disabled" href="user-info.php">Thông tin cá nhân</a>
                    </li>
                    
                    <li class="nav-item active">
                        <a id="logout" class="nav-link disabled" href="#" onclick="logout()">Đăng xuất</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="flex-1 container-fluid" style="background-color:#eee">
        <div class="container">
            <div class="row">
            <div class="col-md-3 d-none d-md-flex flex-column my-3">
                    <div class="border-lg-round p-4">
                        <div class="border-m-round overflow-hidden shadow" style="width: 80px;height:80px;">
                            <img style="width: 80px;height:80px;" class="img-fluid"
                                src="assets/images/placeholder.png" />
                        </div>
                        <p class="h3 my-4">Nguyen Thi A</p>
                        <div class="text-muted"><span class="text-center text-x-small"
                                style="display:inline-block; width: 30px"><i class="fas fa-phone"></i></span>098766555
                        </div>
                        <div class="text-muted"><span class="text-center text-x-small"
                                style="display:inline-block; width: 30px"><i
                                    class="fas fa-envelope"></i></span>abc@gmail.com</div>

                        <div class="w-100 border-bottom my-3"></div>
                        
                    </div>

                    

                </div>
                <div class="col-9 my-3">
                    <div onclick="showUserForm()" class="d-flex flex-row justify-content-between cursor-pointer">
                        <p class="h5 bold mb-3">Danh sách KH</p>
                        <p>Tạo mới <i class="fas fa-plus-circle text-small text-primary"></i></p>
                    </div>
                    <div id="main" class="border-lg-round overflow-hidden">
                        <!-- <div class="g-signin2" data-onsuccess="onSignIn"></div> -->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Mã</th>
                                    <th scope="col">Tên</th>
                                    <th scope="col">Địa chỉ</th>
                                    <th scope="col">SĐT</th>
                                    <th scope="col">Ngày sinh</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>

                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id='form-update' class="position-absolute w-100 h-100 bg-blur d-none justify-content-center align-items-center">
         
    </div>



    <div class="w-100 py-3 pt-2">
        <footer class="container">
            <a href="index.html">
                LOGO
            </a>
            <div class="d-flex justify-content-end align-items-center text-secondary">
                <p class="pr-3 text-x-small ">Hotline: <u>1900 1009</u></p>
                <p class="pr-3 text-x-small ">Email: <u>Hotro@email.com</u></p>
                <p>
                    <i class="fab fa-facebook mr-1"></i>
                    <i class="fab fa-twitter"></i>
                </p>
            </div>

        </footer>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
    
    <script type="text/javascript">
        getUserList()
    </script>
</body>

</html>